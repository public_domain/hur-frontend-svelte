# Hur profile to toml using Svelte

This is a tool for freelancers to create their real free profile and export it to a `toml` file so they can host it anywhere. 

## How it works

It's basically a form that allows user to enter his information and services and generate a toml file.

It's used in the [Hur project](https://gitlab.com/uak/hur/) and [Hur website](https://gitlab.com/uak/hur_website/) projects.

## Screenshot

![User filling the Hur form](https://gitlab.com/uak/hur-frontend-svelte/-/raw/master/images/create_profile.gif)

## License

- AGPL v3
