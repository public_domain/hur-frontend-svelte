import {
  dictionary,
  locale,
  _
} from 'svelte-i18n';
function setupI18n({
  withLocale: _locale
} = {
  withLocale: 'en'
}) {
  dictionary.set({
    en: {
      "home": {
        "topic": "Svelte Localization Tutorial",
        "name": "Name",
        "description": "Describe your self",
        "links": "Links",
        "payment_methods": "Payment Methods",
        "add_service": "Add your services",
        "image": "image",
        "service_tags": "Service tags",
        "profile_image": "Link to profile image",
        "service_image": "Service image",
        "hour_price": "Hour Price",
        "service_price": "Service Price",
        "payment_methods": "Payment methods",
        "toml_content":"Toml content",
        "profile":"Profile",
        "add": "Add",
      },
      "header": {
        "home": "Home",
        "contact": "Contact Us",
        "about": "About"
      }
    },
    ar: {
      "home": {
        "topic": "البرنامج التعليمي Svelte Localization",
        "name": "الإسم",
        "description": "صف نفسك",
        "links": "روابط",
        "payment_methods": "وسائل الدفع",
        "add_service": "أضف خدماتك",
        "image": "صورة",
        "service_tags": "وسوم الخدمات",
        "profile_image": "رابط لصورة",
        "service_image": "صورة الخدمة",
        "hour_price": "سعر الساعة",
        "service_price": "سعر الخدمة",
        "toml_content":"محتوى تومل",
        "profile":"بياناتك",
        "add": "أضف"
      },
      "header": {
        "home": "الصفحة الرئيسية",
        "contact": "اتصل",
        "about": "حول"
      }
    },
  });
  locale.set(_locale);
}
export {
  _,
  setupI18n
};